﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DeviceManager;
//using HalconDotNet;
using SampleEnsenso.DeviceMgr;
using System.Threading;

namespace SampleEnsenso
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public EnsensoSensor Sensor { get; set; }
        public DeviceManager.RunEnsensoTest Test { get; set; } = new RunEnsensoTest();
        protected ILog Log;
        //public HWindow hWindow { get; set; }
        public MainWindow()
        {
            InitializeComponent();
            Log = new Log("UI");
        }

        private void Sensor_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "CurrentImage")
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    MainHWindow.HalconWindow.DispImage(Sensor.CurrentImage);
                });
            }
            else if (e.PropertyName == "CamCon")
            {
                if (Sensor.CamCon)
                {
                    Sensor.Setup();
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        RadioConnected.IsChecked = true;
                    });
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        RadioConnected.IsChecked = false;
                    });
                }
            }
        }

        private void Test_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "TestRunning")
            {
                if (Test.TestRunning)
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        RadioTest.IsChecked = true;
                    });
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        RadioTest.IsChecked = false;
                    });
                }
            }
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {

            Task.Factory.StartNew(
                () =>
                {
                    if (Sensor == null)
                    {
                        Sensor = new EnsensoSensor(ConfigurationManager.AppSettings.Get("EnsensoName"), "../../../Procedures", "EncodeStringsOnImageChannel");
                        Sensor.PropertyChanged += Sensor_PropertyChanged;

                    }
                    Sensor.Start();
                });
                
        }

        
        private void Disconnect_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Task.Factory.StartNew(()=>Sensor.Stop());//Disconnect the Ensenso
            });
        }
        private void Grab_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Sensor.TriggerCamera();//Method to call halcon framegrabber.grabdata
            });
        }

        private void MainDispWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //hWindow = new HWindow();
            //hWindow = MainDispWindow.HalconWindow; 
        }

        private void StopTest_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                if (Test.TestRunning)
                {
                    Test.StopTest();
                    Log.I("Test Stopped");
                }
                else
                {
                    Log.I("No Test to Stop");
                };
            });
        }
        private void StartTest_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                if (Sensor.CamCon && !Test.TestRunning)
                {
                    
                        Test = new RunEnsensoTest(Sensor);
                        Test.PropertyChanged += Test_PropertyChanged;
                        Test.RunTest();
                    
                }
                else if(!Sensor.CamCon)
                {
                    Log.I("No Sensor Connected to Start Test On");
                }
                else if (Test.TestRunning)
                {
                    Log.I("Test Already Running");
                }
            });
            
            
        }
    }
}
