﻿//-----------------------------------------------------------------------
//Ensenso.cs//
// Copyright © 2018, Crandall Engineering, LLC
// All rights reserved.
// http://www.crandallce.com 
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows;
using System.Timers;
using HalconDotNet;
using NLog;

namespace SampleEnsenso.DeviceMgr
{
    public class Ensenso : IDisposable, INotifyPropertyChanged
    {
        #region Properties
        protected ILog Log;
        /// <summary>
        /// Property for cameraID used to open framgrabber
        /// </summary>
        private string _cameraID;

        public string CameraID
        {
            get { return _cameraID; }
            set { _cameraID = value; }
        }
        /// <summary>
        /// Property to store the software camera trigger set by a method in child 
        /// </summary>
        private bool _cameraTrigger;

        public bool CameraTrigger
        {
            get { return _cameraTrigger; }
            set { _cameraTrigger = value; }
        }
        /// <summary>
        /// Property camera connection parent can subscribe
        /// </summary>
        private bool _camCon;

        public bool CamCon
        {
            get { return _camCon; }
            private set
            {
                if (_camCon != value)
                {
                    _camCon = value;
                    OnPropertyChanged();
                }
            }
        }
        /// <summary>
        /// Property that holds the halcon fram grabber pointer used in inheritor class in halcon action
        /// </summary>
        public HFramegrabber _grabber;

        public HFramegrabber Grabber
        {
            get { return _grabber; }
            set { _grabber = value; }
        }
        /// <summary>
        /// Propety to store the latest image from the camera a parent class can subscribe
        /// </summary>
        ReaderWriterLockSlim imageLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        private HImage _currentImage = new HImage();
        public HImage CurrentImage 
        {
            get { return ReadWriteLockSlimHelper<HImage>.GetReadWriteLock(imageLock, _currentImage); }
            set
            {
                if (_currentImage != value)
                {
                    ReadWriteLockSlimHelper<HImage>.SetReadWriteLock(imageLock, ref _currentImage,value);
                    OnPropertyChanged();
                }
            }
        }

        ReaderWriterLockSlim image2DLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        private HImage _current2DImage = new HImage();
        public HImage Current2DImage
        {
            get { return ReadWriteLockSlimHelper<HImage>.GetReadWriteLock(image2DLock, _current2DImage); }
            set
            {
                if (_current2DImage != value)
                {
                    ReadWriteLockSlimHelper<HImage>.SetReadWriteLock(image2DLock, ref _current2DImage, value);
                    OnPropertyChanged();
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        ReaderWriterLockSlim regionLock = new ReaderWriterLockSlim(LockRecursionPolicy.SupportsRecursion);
        private HRegion _currentRegion = new HRegion();
        public HRegion CurrentRegion
        {
            get { return ReadWriteLockSlimHelper<HRegion>.GetReadWriteLock(regionLock, _currentRegion); }
            set
            {
                if (_currentRegion != value)
                {
                    ReadWriteLockSlimHelper<HRegion>.SetReadWriteLock(regionLock,ref _currentRegion,value);
                    OnPropertyChanged();
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private HXLDCont _currentContour;
        public HXLDCont CurrentContour
        {
            get { return _currentContour; }
            set
            {
                if (_currentContour != value)
                {
                    _currentContour = value;
                    OnPropertyChanged();
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private HTuple _currentData;
        public HTuple CurrentData
        {
            get { return _currentData; }
            set
            {
                if (_currentData != value)
                {
                    _currentData = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Property to store the exposure time used in halcon action
        /// </summary>
        private int _exposureTime;

        public int ExposureTime
        {
            get { return _exposureTime; }
            set { _exposureTime = value; }
        }

        private long _debugImageAcquisitionTime;

        public long DebugImageAcquisitionTime
        {
            get { return _debugImageAcquisitionTime; }
            set
            {
                _debugImageAcquisitionTime = value;
                OnPropertyChanged();
            }
        }
        private System.Diagnostics.Stopwatch _imageAcquisitionStopWatch;
        public System.Diagnostics.Stopwatch ImageAcquisitionStopWatch
        {
            get { return _imageAcquisitionStopWatch; }
            set
            {
                _imageAcquisitionStopWatch = value;
                OnPropertyChanged();
            }
        }
        public bool IsSoftwareTriggered { get; set; }

        private bool _IsLiveView;
        public bool IsLiveView
        {
            get { return _IsLiveView; }
            set
            {
                if (_IsLiveView != value)
                {
                    _IsLiveView = value;
                    OnPropertyChanged();
                }
            }
        }

        private bool CameraStopFlag { get; set; }
        private bool bIsReconnectEnabled { set; get; }
        private bool bSaveImages { get; set; }
        #endregion

        #region Constructor

        public Ensenso()
        {
            try
            {
                Log = new Log("Ensenso");
                IsSoftwareTriggered = true;
                IsLiveView = false;
                bSaveImages = true;
                CurrentImage = new HImage();
                CurrentRegion = new HRegion();
                CurrentContour = new HXLDCont();
                Grabber = new HFramegrabber();
                NxLibRoot = new HFramegrabber();
                ImageAcquisitionStopWatch = new System.Diagnostics.Stopwatch();
                WatchDog = new System.Timers.Timer(5000.00);
                WatchDog.Elapsed += WatchDog_Elapsed;
                WatchDog.Enabled = true;
                WatchDog.AutoReset = false;
                WatchDog.Stop();

            }
            catch (Exception ex)
            {
                Log.I($"Exception Thrown in Ensenso Construction: {ex}");
            }
        }

        private void WatchDog_Elapsed(object sender, ElapsedEventArgs e)
        {
            Log.E($"Ensenso Timeout waiting for image frame to return");
            if (CamCon)
            {
                Grabber.SetFramegrabberParam("do_export_debug_buffer", $"../../../Logs/ensensolog{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Hour}.nxlog");
            }
            //ResetEnsenso();
        }
        #endregion

        private CancellationTokenSource _CameraTokenSource;
        private CancellationToken _CameraCancellationToken;
        private Task _CameraTask;
        private System.Timers.Timer WatchDog;
        public HFramegrabber NxLibRoot;
        /// <summary>
        /// Method to start the camera task. Is activated from a parent class or the inheritor class
        /// </summary>
	    private void startCameraTask()
        {
            _CameraTokenSource = new CancellationTokenSource();
            _CameraCancellationToken = _CameraTokenSource.Token;
            if (_CameraTask != null && (_CameraTask.Status == TaskStatus.Running || _CameraTask.Status == TaskStatus.WaitingToRun || _CameraTask.Status == TaskStatus.WaitingForActivation))
            {
                Log.I("Task has attempted to start while already running");
            }
            else
            {
                _CameraTask = new Task(() => CameraTaskManager(), _CameraCancellationToken, TaskCreationOptions.LongRunning);
                _CameraTask.Start();
            }
        }

        private int pollTime = 100; //ms
        /// <summary>
        /// Camera Task Wrapper.
        /// Opens the framergrabber, checks for a software or hardware trigger, then grabs an image, invokes the halcon Action and reset camera trigger
        /// </summary>
        private void CameraTaskManager()
        {
            try
            {
                bool cancelled = _CameraCancellationToken.WaitHandle.WaitOne(pollTime);
                CheckIfCancelled(_CameraCancellationToken);
                Log.I($"Attempting new ensenso connection to {CameraID}");
                NxLibRoot.OpenFramegrabber("Ensenso-NxLib", 0, 0, 0, 0, 0, 0, "default", 0, "Raw", -1,
                        "false", "Item", "/", 0, 0);
                Grabber.OpenFramegrabber("Ensenso-NxLib", 0, 0, 0, 0, 0, 0, "default", 0, "Rectified", -1,
                        "false", "Stereo", CameraID, 0, -1);
                CamCon = true;
                Log.I($"{CameraID} connected and task is running");
                if (IsSoftwareTriggered)
                {
                    while (true)
                    {
                        if (IsLiveView)
                        {
                            EngageLiveView();
                        }
                        else
                        {
                            GrabImageOnSoftwareTrigger();
                        }
                    }
                }
                else
                {
                    while (true)
                    {
                        GrabImageOnHardwareTrigger();
                    }
                } 
            }
            catch (HalconException hex)
            {
                if (hex.GetErrorCode() == 5335 || hex.GetErrorCode() == 5302)// is HalconDotNet.HOperatorException)
                {
                    if (!CameraStopFlag)
                        bIsReconnectEnabled = true;
                    else
                        CameraStopFlag = false;
                }
                Task.Factory.StartNew(() => stopCamera());
                Log.E($"Can't Communicate with {CameraID} ErrorCode: {hex.GetErrorCode()} Message:{hex}");
                //Grabber.SetFramegrabberParam(new HTuple("do_export_debug_buffer"), new HTuple("append", "C:/Cluebra/logs/file.nxlog"));
                CheckIfCancelled(_CameraCancellationToken);
            }
            catch (NullReferenceException nex)
            {
                Log.E($"Null Reference Missed in Ensenso Class {nex.Message}");
            }
            
        }

        private void CheckIfCancelled(CancellationToken _CancellationToken)
        {
            if (_CancellationToken.IsCancellationRequested)
            {
                //add cleanup items, if necessary
                Log.I($"{CameraID} Ensenso Task Cancelled");
                _CancellationToken.ThrowIfCancellationRequested();
            }
        }

        #region Image Acquisition

        private void EngageLiveView()
        {
            CheckIfCancelled(_CameraCancellationToken);
            ImageAcquisitionStopWatch.Start();
            CurrentImage = Grabber.GrabImageAsync(-1);
            ImageAcquisitionStopWatch.Stop();
            DebugImageAcquisitionTime = ImageAcquisitionStopWatch.ElapsedMilliseconds;
            ImageAcquisitionStopWatch.Reset();
        }
        private void GrabImageOnHardwareTrigger()
        {
            CheckIfCancelled(_CameraCancellationToken);
            ImageAcquisitionStopWatch.Start();
            CurrentImage = Grabber.GrabImageAsync(-1);
            ImageAcquisitionStopWatch.Stop();
            DebugImageAcquisitionTime = ImageAcquisitionStopWatch.ElapsedMilliseconds;
            ImageAcquisitionStopWatch.Reset();
        }

        private void GrabImageOnSoftwareTrigger()
        {
                
                CheckIfCancelled(_CameraCancellationToken);
                if (CameraTrigger)
                {
                    
                    Grabber.SetFramegrabberParam("do_export_debug_buffer", $"../../../Logs/ensensolog{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Hour}.nxlog");
                    CameraTrigger = false;
                    ImageAcquisitionStopWatch.Start();
                    WatchDog.Start();
                    if(CurrentRegion != null) CurrentRegion.Dispose();
                    if(CurrentContour !=null) CurrentContour.Dispose();
                    HRegion _ensensoRegion = new HRegion();
                    HXLDCont _ensensoCont = new HXLDCont();
                    HTuple _ensensoData = new HTuple();
                    HImage _ensensoImage = new HImage();
                    HImage _ensenso2DImage = new HImage();
                    _ensensoImage = Grabber.GrabData(out _ensensoRegion, out _ensensoCont, out _ensensoData);
                    _ensenso2DImage = Grabber.GrabImage();
                    Grabber.SetFramegrabberParam("do_export_debug_buffer", $"../../../Logs/ensensolog{DateTime.Now.Year}-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Hour}.nxlog");
                    //Grabber.SetFramegrabberParam("do_export_debug_buffer", "C:/Cluebra/Logs/ensensolog.nxlog");
                    DebugImageAcquisitionTime = ImageAcquisitionStopWatch.ElapsedMilliseconds;
                    Log.I($"Ensenso Image Acquision done in:{DebugImageAcquisitionTime} milliseconds");
                    ImageAcquisitionStopWatch.Reset();
                    WatchDog.Stop();
                    Current2DImage = _ensenso2DImage;
                    CurrentRegion = _ensensoRegion;
                    CurrentContour = _ensensoCont;
                    CurrentData = _ensensoData;
                    CurrentImage = _ensensoImage;
                    _ensensoRegion.Dispose();
                    _ensensoCont.Dispose();
                    
                    if (bSaveImages)
                    {
                        SaveImage();
                    }
                }
                else
                {
                    Thread.Sleep(50);
                }
            //}
            //catch (Exception Ex)
            //{
            //    CheckIfCancelled(_CameraCancellationToken);
            //    Log.E($"Exception thrown during image grabbing software trigger: {Ex}");
            //}
            //catch (Exception ex)
            //{
            //    Log.I($"{CameraID} Exception --- {ex.Message}");
            //    CheckIfCancelled(_CameraCancellationToken);
            //}
        }
        private void SaveImage( )
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    //HImage _image = new HImage(CurrentImage);
                    //HImage _image2D = new HImage(Current2DImage);
                    //HRegion _currentRegion = new HRegion(CurrentRegion);
                    ////HOperatorSet.WriteImage(_image, "tiff", 0,);
                
                    //_image.WriteImage("tiff", 0, $"C:/EnsensoImages/EnsensoImage{System.DateTime.UtcNow.Day}_{DateTime.UtcNow.Hour}_{DateTime.UtcNow.Minute}_{DateTime.UtcNow.Second}.tif");
                    //_image2D.WriteImage("tiff", 0, $"C:/EnsensoImages/EnsensoImage2D{System.DateTime.UtcNow.Day}_{DateTime.UtcNow.Hour}_{DateTime.UtcNow.Minute}_{DateTime.UtcNow.Second}.tif");
                    //CurrentRegion.WriteRegion($"C:/EnsensoImages/EnsensoRegion{System.DateTime.UtcNow.Day}_{DateTime.UtcNow.Hour}_{DateTime.UtcNow.Minute}_{DateTime.UtcNow.Second}");
                    //Log.I($"ImageSaved From Ensenso");
                    //_image.Dispose();
                    //_image2D.Dispose();
                    //_currentRegion.Dispose();
                }
                catch (HalconException hex)
                {
                    Log.E($"Halcon Exception thrown during image saving: {hex.Message}");
                }
                catch(Exception ex)
                {
                    
                    Log.E($"Exception thrown during image saving: {ex.Message}");
                }
            });
        }
        #endregion
        /// <summary>
        /// Method to stop the camera task activated from an administrating parent
        /// </summary>
        private void stopCamera()
        {
            CameraStopFlag = true;
            if (_CameraTask != null && (_CameraTask.Status == TaskStatus.Running || _CameraTask.Status == TaskStatus.WaitingToRun || _CameraTask.Status == TaskStatus.WaitingForActivation))
            {
                _CameraTokenSource.Cancel();
                try
                {
                    _CameraTask.Wait();
                }
                catch (AggregateException ae)
                {
                    Log.I($"Exception from StopAcquisitionTasks CameraTask method: {ae.Message}");
                    Log.I($"Disconnecting From Halcon FrameGrabber");
                    Grabber.Dispose();
                    Log.I($"Disconnecting From NXLib Instance");
                    NxLibRoot.Dispose();
                    if(CurrentImage != null)
                    {
                        CurrentImage.Dispose();
                        Current2DImage.Dispose();
                        CurrentContour.Dispose();
                        CurrentRegion.Dispose();
                    }
                    if (ae.InnerException is OperationCanceledException)
                    {
                        Log.I("Error was Cancellation");
                    }
                    CamCon = false;
                    if (bIsReconnectEnabled)
                    {
                        bIsReconnectEnabled = false;
                        Thread.Sleep(5000);
                        Log.I($"Attempting a Reconnect on {CameraID}");
                        startCameraTask();
                    }
                }
            }
        }
        public void Start()
        {
            startCameraTask();
        }
        public void Stop()
        {
            stopCamera();
        }
        public void ToggleLiveView()
        {
            IsLiveView = !IsLiveView;
        }
        /// <summary>
        /// Method to set the camera trigger property activated from an administoring parent
        /// </summary>
        public void TriggerCamera()
        {
            CameraTrigger = true;
            Log.I("Ensenso Camera Triggered");
        }
        #region INofity Implmination
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    if (_CameraTask != null && (_CameraTask.Status == TaskStatus.Running || _CameraTask.Status == TaskStatus.WaitingToRun || _CameraTask.Status == TaskStatus.WaitingForActivation))
                        Task.Factory.StartNew(()=>stopCamera());
                    CurrentImage = null;
                }
                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {

            Dispose(true);

        }
        #endregion

    }
}
