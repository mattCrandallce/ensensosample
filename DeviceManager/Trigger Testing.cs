﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using SampleEnsenso.DeviceMgr;
using SampleEnsenso;
using System.Threading;
using HalconDotNet;

namespace DeviceManager
{
    public class RunEnsensoTest : INotifyPropertyChanged
    {
        public System.Timers.Timer CaptureTimer { get; set; }
        public EnsensoSensor SensorUnderTest { get; set; }
        private Task[] testTasks = new Task[3];
        private int TestCount = 0;
        private CancellationTokenSource tokensource = new CancellationTokenSource();
        private CancellationToken token = new CancellationToken();
        private bool testRunning = false;
        
        public bool TestRunning
        {
            get
            {
                return testRunning;
            }
            set
            {
                if(testRunning != value)
                {
                    testRunning = value;
                    OnPropertyChanged();
                }
                
            }
        }
        protected ILog Log = new Log("Test");
        public RunEnsensoTest()
        {
           
            TestRunning = false;
            try
            {
                token = tokensource.Token;
            }
            catch (Exception ex)
            {
                Log.E($"Exception Thrown setting up tasks {ex.Message}");
            }
            
        }
        public RunEnsensoTest(EnsensoSensor sensor)
        {
            SensorUnderTest = sensor;
            

            for (int i = 0; i < testTasks.Length; i++)
            {
                testTasks[i] = new Task(new Action(() => testthread()), token, TaskCreationOptions.LongRunning);
            }
            

        }
        private bool stopFlag = false;

        public void RunTest()
        {
            try
            {
                foreach (Task _task in testTasks)
                {
                    _task.Start();
                }
            }
            catch (Exception ex)
            {
                Log.E($"Exception Thrown Starting Tests {ex.Message}");
            }
           
            if (SensorUnderTest.CamCon)
            {
                SensorUnderTest.TriggerCamera();
                CaptureTimer = new System.Timers.Timer(1500);
                CaptureTimer.AutoReset = true;
                CaptureTimer.Elapsed += CaptureTimer_Elapsed;
                CaptureTimer.Start();
                TestRunning = true;
            }
        }

        private void CaptureTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (!stopFlag && SensorUnderTest.CamCon && TestCount != 10 && TestCount != 50)
            {
                SensorUnderTest.TriggerCamera();
                TestCount++;
                Log.I($"{TestCount} Triggers Completed");
            }
            else if (!stopFlag && SensorUnderTest.CamCon && TestCount == 10)
            {
                CaptureTimer.Stop();
                SensorUnderTest.Start();
                SensorUnderTest.TriggerCamera();
                TestCount++;
                CaptureTimer.Start();
                
            }
            else if (!stopFlag && SensorUnderTest.CamCon && TestCount == 50)
            {
                CaptureTimer.Stop();
                DisconnectReconnectTest();
                SensorUnderTest.TriggerCamera();
                TestCount = 0;
                CaptureTimer.Start();
            }
            else if (stopFlag)
            {
                CaptureTimer.AutoReset = false;
                this.CaptureTimer.Stop();
                TestRunning = false;
                stopFlag = false;
                TestCount = 0;
            }
            else if (!SensorUnderTest.CamCon)
            {
                Log.I($"Camera:{SensorUnderTest.CameraID} not connected stopping test");
                CaptureTimer.AutoReset = false;
                this.CaptureTimer.Stop();
                TestRunning = false;
                TestCount = 0;
            }
            
        }
        public void StopTest()
        {
            stopFlag = true;
            while (testRunning)
            {
                Thread.Sleep(25);
            }
            tokensource.Cancel();
            try
            {
                
                Task.WaitAll(testTasks);
            }
            catch(AggregateException agex)
            {
                if (agex.InnerException is OperationCanceledException)
                {
                    Log.I($"TestThreads Cancelled Sucessfully");
                }
            }

        }
        private void DisconnectReconnectTest()
        {
            Task.Factory.StartNew(() => SensorUnderTest.Stop());
            while (SensorUnderTest.CamCon)
            {
                Thread.Sleep(100);
            }
            Random random = new Random(DateTime.UtcNow.Millisecond);
            int waitint = random.Next();
            Log.I($"Staying Disconnected for {waitint % 2500}");
            Thread.Sleep(waitint % 2500);//Stay Disconnected for a random amount of time from 0 to 2500 ms
            Task.Factory.StartNew(() => SensorUnderTest.Start());
            while (!SensorUnderTest.CamCon)
            {
                Thread.Sleep(100);
            }
            //Done disconnecting and reconecting
            
        }

        private void testthread()
        {
            while (true)
            {
                
                if (!token.IsCancellationRequested)
                {
                    
                    try
                    {
                        if (!SensorUnderTest.CameraTrigger)
                        {
                            Log.I($"Fake Work Started");
                            HImage workingImage = new HImage(SensorUnderTest.Current2DImage);
                            HImage working3DImage = new HImage(SensorUnderTest.CurrentImage);
                            //2D Work
                            HTuple width = new HTuple();
                            HTuple height = new HTuple();
                            HObject gausImage = new HObject();
                            HTuple size = new HTuple(5);
                            HOperatorSet.GetImageSize(workingImage, out width, out height);
                            HOperatorSet.GaussFilter(workingImage, out gausImage, size);
                            gausImage.Dispose();
                            workingImage.Dispose();
                            //3D work to take process
                            HObject Channel1 = new HObject();
                            HObject Channel2 = new HObject();
                            HObject Channel3 = new HObject();
                            HOperatorSet.Decompose3(working3DImage, out Channel1, out Channel2, out Channel3);
                            HTuple objmodid = new HTuple();
                            HOperatorSet.XyzToObjectModel3d(Channel1, Channel2, Channel3, out objmodid);
                            HTuple sampledobjmodid = new HTuple();
                            HTuple empty = new HTuple();
                            HOperatorSet.SampleObjectModel3d(objmodid, "fast", 0.008, empty, empty, out sampledobjmodid);
                            HOperatorSet.ClearObjectModel3d(sampledobjmodid);
                            HOperatorSet.ClearObjectModel3d(objmodid);
                            working3DImage.Dispose();
                            Thread.Sleep(2000);
                        }
                       
                    }
                    catch(HalconException hex)
                    {
                        Log.E($"Halcon exception thrown doing fake work: {hex.Message}");
                        Thread.Sleep(5000);
                    }
                    
                }
                else
                {
                    token.ThrowIfCancellationRequested();
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
