﻿//-----------------------------------------------------------------------
//ReadWriteLockSlimHelper.cs//
// Copyright © 2018, Crandall Engineering, LLC
// All rights reserved.
// http://www.crandallce.com 
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SampleEnsenso
{
    /// <summary>
    /// A generic read/write lock static class. This lock allows multiple threads to read a property, 
    /// but only a single thread to write to it.
    /// </summary>
    /// <typeparam name="T">The type of the variable to read or write</typeparam>
    public static class ReadWriteLockSlimHelper<T>
    {
        public static T GetReadWriteLock(ReaderWriterLockSlim locker, T member)
        {
            try
            {
                locker.EnterReadLock();
                return member;
            }
            finally
            {
                locker.ExitReadLock();
            }
        }

        public static void SetReadWriteLock(ReaderWriterLockSlim locker, ref T member, T newValue)
        {
            try
            {
                locker.EnterWriteLock();
                if (!member.Equals(newValue)) member = newValue;
            }
            finally
            {
                locker.ExitWriteLock();
            }
        }
    }
}
