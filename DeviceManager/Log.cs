﻿//-----------------------------------------------------------------------
//Log.cs//
// Copyright © 2018, Crandall Engineering, LLC
// All rights reserved.
// http://www.crandallce.com 
//-----------------------------------------------------------------------


using System;
using NLog;

namespace SampleEnsenso
{
    public class Log : ILog
    {
        private readonly ILogger _logger;

        public Log(string name)
        {
            _logger = LogManager.GetLogger(name);
        }

        /// <summary>
        ///     Add a trace log entry.
        /// </summary>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void T(string fmt, params object[] args)
        {
            _logger.Trace(fmt, args);
        }

        /// <summary>
        ///     Add a trace log entry with exception information.
        /// </summary>
        /// <param name="ex">Exception object</param>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void T(Exception ex, string fmt, params object[] args)
        {
            _logger.Trace(ex, fmt, args);
        }

        /// <summary>
        ///     Add a debug log entry.
        /// </summary>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void D(string fmt, params object[] args)
        {
            _logger.Debug(fmt, args);
        }

        /// <summary>
        ///     Add a debug log entry with exception information.
        /// </summary>
        /// <param name="ex">Exception object</param>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void D(Exception ex, string fmt, params object[] args)
        {
            _logger.Debug(ex, fmt, args);
        }

        /// <summary>
        ///     Add an info log entry.
        /// </summary>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void I(string fmt, params object[] args)
        {
            _logger.Info(fmt, args);
        }

        /// <summary>
        ///     Add an info log entry with exception information.
        /// </summary>
        /// <param name="ex">Exception object</param>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void I(Exception ex, string fmt, params object[] args)
        {
            _logger.Info(ex, fmt, args);
        }

        /// <summary>
        ///     Add a warning log entry.
        /// </summary>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void W(string fmt, params object[] args)
        {
            _logger.Warn(fmt, args);
        }

        /// <summary>
        ///     Add a warning log entry with exception information.
        /// </summary>
        /// <param name="ex">Exception object</param>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void W(Exception ex, string fmt, params object[] args)
        {
            _logger.Warn(ex, fmt, args);
        }

        /// <summary>
        ///     Add an error log entry.
        /// </summary>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void E(string fmt, params object[] args)
        {
            _logger.Error(fmt, args);
        }

        /// <summary>
        ///     Add an error log entry with exception information.
        /// </summary>
        /// <param name="ex">Exception object</param>
        /// <param name="fmt">Format string</param>
        /// <param name="args">Format arguments</param>
        [StringFormatMethod("fmt")]
        public void E(Exception ex, string fmt, params object[] args)
        {
            _logger.Error(ex, fmt, args);
        }
    }

    /// <summary>
    /// Indicates that the marked method builds string by format pattern and (optional) arguments.
    /// Parameter, which contains format string, should be given in constructor. The format string
    /// should be in <see cref="string.Format(IFormatProvider,string,object[])"/>-like form.
    /// </summary>
    /// <example><code>
    /// [StringFormatMethod("message")]
    /// void ShowError(string message, params object[] args) { /* do something */ }
    /// 
    /// void Foo() {
    ///   ShowError("Failed: {0}"); // Warning: Non-existing argument in format string
    /// }
    /// </code></example>
    [AttributeUsage(
        AttributeTargets.Constructor | AttributeTargets.Method |
        AttributeTargets.Property | AttributeTargets.Delegate)]
    public sealed class StringFormatMethodAttribute : Attribute
    {
        /// <param name="formatParameterName">
        /// Specifies which parameter of an annotated method should be treated as format-string
        /// </param>
        public StringFormatMethodAttribute(string formatParameterName)
        {
            FormatParameterName = formatParameterName;
        }

        public string FormatParameterName { get; private set; }
    }
}

