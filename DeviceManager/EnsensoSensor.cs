﻿//-----------------------------------------------------------------------
//ensensosensor.cs//
// Copyright © 2018, Crandall Engineering, LLC
// All rights reserved.
// http://www.crandallce.com 
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;

namespace SampleEnsenso.DeviceMgr
{
    public class EnsensoSensor:Ensenso
    {
        protected ILog Log;
        private HDevProcedure ProductImageEncoder;
        private HDevEngine HEng;
        public EnsensoSensor(string _cameraID, string _procedurepath, string _procedurename)
        {
            //TODO:Extract config values to pass to ensenso camera
            Log = new Log("Ensenso");
            IsSoftwareTriggered = true;
            ExposureTime = Convert.ToInt32(2400); //2400;
            CameraID = _cameraID;
            HEng = new HDevEngine();
            HEng.SetProcedurePath(_procedurepath);
            ProductImageEncoder = new HDevProcedure(_procedurename);
            //Start();
        }
        /// <summary>
        /// Method To set Process Specific FrameGrabber Parameters is triggered by the camera connection
        /// </summary>
        public void Setup()
        {
            try
            {
                Grabber.SetFramegrabberParam("Parameters/DisparityMap/StereoMatching/MinimumDisparity", -127);
                Grabber.SetFramegrabberParam("Parameters/DisparityMap/StereoMatching/NumberOfDisparities", 192);
                Grabber.SetFramegrabberParam("Parameters/Capture/TargetBrightness", 40);
                Grabber.SetFramegrabberParam("Parameters/Capture/GainBoost", "true");
                Grabber.SetFramegrabberParam("Parameters/Capture/Projector", "true");
                Grabber.SetFramegrabberParam("Parameters/Capture/Hdr", "true");
                Grabber.SetFramegrabberParam("Parameters/Capture/AutoExposure", "false");
                Grabber.SetFramegrabberParam("Parameters/Capture/Exposure", 1);
                NxLibRoot.SetFramegrabberParam("Debug/Level", "Debug");
                //Grabber.SetFramegrabberParam("Parameters/Capture/Exposure", 2.85);
                HTuple TopLeft = new HTuple(245, 350);
                HTuple AOITopLeft = new HTuple("Parameters/DisparityMap/AreaOfInterest/LeftTop");
                Grabber.SetFramegrabberParam(AOITopLeft, TopLeft);
                HTuple BottomRight = new HTuple(800, 1050);
                HTuple AOIBottomRight = new HTuple("Parameters/DisparityMap/AreaOfInterest/RightBottom");
                Grabber.SetFramegrabberParam(AOIBottomRight,BottomRight);
            }
            catch (HalconException hex)
            {
                Log.E($"Halcon Exception Thrown While Setting Ensenso Settings: {hex.Message}");
            }
            catch(Exception ex)
            {
                Log.E($"Exception Thrown While Setting Ensenso Settings: {ex.Message}");
            }

        }
        public void ImagePackagerandSaver(string[] upcs)
        {
            try
            {
                //Copy Image Elements
                HImage _image = new HImage(CurrentImage);
                HImage _image2D = new HImage(Current2DImage);
                _image2D.ConvertImageType("real");
                HRegion _currentRegion = new HRegion(CurrentRegion);
                HObject _reducedImage = new HObject();
                HOperatorSet.ReduceDomain(_image, _currentRegion, out _reducedImage);
                HObject _tempchannel1 = new HObject();
                HObject _tempchannel2 = new HObject();
                HObject _tempchannel3 = new HObject();
                HOperatorSet.Decompose3(_reducedImage, out _tempchannel1, out _tempchannel2, out _tempchannel3);
                HObject PackagedImage = new HObject();
                //Cobine the 3 parts of the image into one image
                HOperatorSet.Compose4(_tempchannel1, _tempchannel2, _tempchannel3,_image2D, out PackagedImage);
                HDevProcedureCall _mImageEncoderCall = ProductImageEncoder.CreateCall();
                //Pass Inputs
                _mImageEncoderCall.SetInputIconicParamObject("i_Image", PackagedImage);
                _mImageEncoderCall.SetInputCtrlParamTuple("i_InputStrings", upcs);
                //Execute
                _mImageEncoderCall.Execute();
                //Get and Save Final Image
                HImage qOutput = _mImageEncoderCall.GetOutputIconicParamImage("q_Output");
                //ImageManager.SaveImage(qOutput,"Ensenso",500);
                //qOutput.WriteImage("tiff", 255, $"C:/EnsensoImages/EnsensoImage_test{System.DateTime.UtcNow.Day}_{DateTime.UtcNow.Hour}_{DateTime.UtcNow.Minute}_{DateTime.UtcNow.Second}.tif");
                Log.I($"Ensenso Image saved with time stamp{System.DateTime.UtcNow.Day}_{DateTime.UtcNow.Hour}_{DateTime.UtcNow.Minute}_{DateTime.UtcNow.Second}");
                //Dispose of Halcon Handles
                _image.Dispose();
                _image2D.Dispose();
                _currentRegion.Dispose();
                PackagedImage.Dispose();
                _mImageEncoderCall.Dispose();
                _tempchannel1.Dispose();
                _tempchannel2.Dispose();
                _tempchannel3.Dispose();
                qOutput.Dispose();
            }
            catch (HalconException hex)
            {
                Log.E($"Exception Thrown Packaging Ensenso Image: {hex.Message}");
            }
            catch (Exception ex)
            {
                Log.E($"Exception Thrown Packaging Ensenso Image: {ex.Message}");
            }

        }
    }
}
